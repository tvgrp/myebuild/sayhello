execname=sayhello
source=sayhello.sh.in

msg=$(execname)

.PHONY: all
all: $(execname)

$(execname): $(source)
	sed 's/%%HELLOMESSAGE%%/$(msg)/' $(source) > $(execname)

prefix = /usr
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin

.PHONY: install
install: $(execname)
	mkdir -p $(DESTDIR)/$(bindir)
	install $< $(DESTDIR)/$(bindir)/$(execname)

.PHONY: uninstall
uninstall:
	$(RM) $(DESTDIR)/$(bindir)/$(execname)

.PHONY: clean
clean:
	$(RM) $(execname)
